<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Dim\Mollie\Controllers\DimMollieApiController;

Route::get('/mollie/test', [DimMollieApiController::class, "test"]);
