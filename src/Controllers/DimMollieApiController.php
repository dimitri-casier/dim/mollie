<?php

namespace Dim\Mollie\Controllers;

use Dim\Core\Controllers\Controller;
use Illuminate\Http\Request;

class DimMollieApiController extends Controller
{
    public function test()
    {
        return "api: mollie package loaded";
    }
}
