<?php

namespace Dim\Mollie;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;


class DimMollieServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot()
    {
        $this->bootRoutes();
    }

    protected function bootRoutes()
    {

        Route::group(
            [
                'prefix' => 'api',
                'middleware' => 'api',
            ],
            function () {
                $this->loadRoutesFrom($this->packageDir('routes/api.php'));
            }
        );

        Route::group(
            [
                'middleware' => 'web',
            ],
            function () {
                $this->loadRoutesFrom($this->packageDir('routes/web.php'));
            }
        );
    }

    protected function packageDir($path)
    {
        return base_path('vendor/dim/mollie/') . $path;
    }
}
